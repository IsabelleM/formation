DROP DATABASE IF EXISTS Formation;

CREATE DATABASE IF NOT EXISTS Formation;

USE Formation

CREATE TABLE apprenant (
  id_appr int PRIMARY KEY,
  name varchar(56),
  email varchar(56),
  adress text,
  phone varchar(30)
);

CREATE TABLE materiel (
  id int PRIMARY KEY,
  type varchar(255),
  quantity int,
  id_appr int,
  FOREIGN KEY (id_appr) REFERENCES apprenant(id_appr) on delete CASCADE
);

CREATE TABLE projet (
  id_projet int PRIMARY KEY,
  nom varchar(56),
  date_debut date,
  date_fin date
);

CREATE TABLE groupe (
  id_group int PRIMARY KEY,
  nom varchar(56),
  id_projet int,
  FOREIGN KEY (id_projet) REFERENCES projet(id_projet) on delete CASCADE
);

CREATE TABLE appr_group (
  id_appr int,
  id_group int,
  FOREIGN KEY (id_appr) REFERENCES apprenant(id_appr) on delete CASCADE,
  FOREIGN KEY (id_group) REFERENCES groupe(id_group) on delete CASCADE 
);

CREATE TABLE competences (
  id_comp int PRIMARY KEY,
  nom varchar(56),
  acquises bool
);

CREATE TABLE comp_projet (
  id_comp int,
  id_projet int,
  FOREIGN KEY (id_comp) REFERENCES competences(id_comp) on delete CASCADE,
  FOREIGN KEY (id_projet) REFERENCES projet(id_projet) on delete CASCADE
);


CREATE TABLE appr_comp (
  id_appr int,
  id_comp int,
  FOREIGN KEY (id_appr) REFERENCES apprenant(id_appr) on delete CASCADE,
  FOREIGN KEY (id_comp) REFERENCES competences(id_comp) on delete CASCADE
);

CREATE TABLE presence (
  id_pres int PRIMARY KEY,
  signature blob,
  date_debut date,
  date_fin date
);

CREATE TABLE appr_pres (
  id_appr int,
  id_pres int,
  FOREIGN KEY (id_appr) REFERENCES apprenant(id_appr) on delete CASCADE,
  FOREIGN KEY (id_pres) REFERENCES presence(id_pres) on delete CASCADE
);

CREATE TABLE diplomes (
  id_diplome int PRIMARY KEY,
  nom text,
  date_obtention date,
  acquis bool
);

CREATE TABLE appr_dip(
  id_appr int,
  id_diplome int,
  FOREIGN KEY (id_appr) REFERENCES apprenant(id_appr) on delete CASCADE,
  FOREIGN KEY (id_diplome) REFERENCES diplomes(id_diplome) on delete CASCADE
);

-- Insert rows apprenant
insert into apprenant (id_appr, name, email, adress, phone) values (1, 'Sena Essame', 'sessame0@engadget.com', '61 Glacier Hill Road', '228-186-7026');
insert into apprenant (id_appr, name, email, adress, phone) values (2, 'Hi Vannikov', 'hvannikov1@unicef.org', '32846 Waywood Lane', '170-261-5889');
insert into apprenant (id_appr, name, email, adress, phone) values (3, 'Anthea Dungay', 'adungay2@google.nl', '0 Butternut Pass', '470-825-5682');
insert into apprenant (id_appr, name, email, adress, phone) values (4, 'Benjy Conklin', 'bconklin3@odnoklassniki.ru', '1138 Sycamore Point', '649-776-0970');
insert into apprenant (id_appr, name, email, adress, phone) values (5, 'Dionne Cullimore', 'dcullimore4@umn.edu', '76779 Prairieview Parkway', '875-400-3173');
insert into apprenant (id_appr, name, email, adress, phone) values (6, 'Bartholomeo Downage', 'bdownage5@google.de', '07 Grover Terrace', '962-697-0484');
insert into apprenant (id_appr, name, email, adress, phone) values (7, 'Addie Noli', 'anoli6@i2i.jp', '6156 Mendota Junction', '510-273-4443');
insert into apprenant (id_appr, name, email, adress, phone) values (8, 'Tiena Hewson', 'thewson7@amazon.de', '5 Algoma Alley', '299-416-6131');
insert into apprenant (id_appr, name, email, adress, phone) values (9, 'Erl Alps', 'ealps8@skyrock.com', '208 Northfield Point', '966-121-0679');
insert into apprenant (id_appr, name, email, adress, phone) values (10, 'Marianna Bartocci', 'mbartocci9@mail.ru', '92771 Spohn Road', '331-435-5073');
insert into apprenant (id_appr, name, email, adress, phone) values (11, 'Horatio Vampouille', 'hvampouillea@etsy.com', '912 Waywood Street', '356-701-4114');
insert into apprenant (id_appr, name, email, adress, phone) values (12, 'Shelley Crimpe', 'scrimpeb@twitpic.com', '9265 Saint Paul Trail', '939-594-5200');
insert into apprenant (id_appr, name, email, adress, phone) values (13, 'Benjamin Colomb', 'bcolombc@ucsd.edu', '1790 Petterle Circle', '806-169-2070');
insert into apprenant (id_appr, name, email, adress, phone) values (14, 'Tarra Spurdens', 'tspurdensd@trellian.com', '0 Boyd Terrace', '867-995-0391');
insert into apprenant (id_appr, name, email, adress, phone) values (15, 'Sigfried Baggallay', 'sbaggallaye@timesonline.co.uk', '79 Pennsylvania Way', '156-524-9558');
insert into apprenant (id_appr, name, email, adress, phone) values (16, 'Trudie Hanner', 'thannerf@mapquest.com', '13934 Blackbird Junction', '628-780-6247');
insert into apprenant (id_appr, name, email, adress, phone) values (17, 'Kial Chinn', 'kchinng@netlog.com', '1 Eastlawn Avenue', '417-195-9940');
insert into apprenant (id_appr, name, email, adress, phone) values (18, 'Seana Benyan', 'sbenyanh@dmoz.org', '87 Darwin Pass', '730-984-5815');
insert into apprenant (id_appr, name, email, adress, phone) values (19, 'Philis Aughton', 'paughtoni@xrea.com', '26 Victoria Place', '560-551-7824');
insert into apprenant (id_appr, name, email, adress, phone) values (20, 'Salvidor Powe', 'spowej@dyndns.org', '40 Lindbergh Hill', '564-921-5172');
insert into apprenant (id_appr, name, email, adress, phone) values (21, 'Webster Lodwick', 'wlodwickk@vinaora.com', '4 Sunnyside Center', '343-683-4130');
insert into apprenant (id_appr, name, email, adress, phone) values (22, 'Victoir Treverton', 'vtrevertonl@php.net', '69289 Lakewood Gardens Court', '648-947-9226');
insert into apprenant (id_appr, name, email, adress, phone) values (23, 'Cami Rysdale', 'crysdalem@ft.com', '9750 Washington Drive', '789-751-5611');
insert into apprenant (id_appr, name, email, adress, phone) values (24, 'Emalee Lockhart', 'elockhartn@wired.com', '98 American Parkway', '360-105-4403');
insert into apprenant (id_appr, name, email, adress, phone) values (25, 'Adoree Belsher', 'abelshero@apache.org', '3 Debs Street', '953-922-0615');
insert into apprenant (id_appr, name, email, adress, phone) values (26, 'Goldy Muneely', 'gmuneelyp@loc.gov', '0813 Spohn Court', '928-906-4418');
insert into apprenant (id_appr, name, email, adress, phone) values (27, 'Carling Boddie', 'cboddieq@uol.com.br', '59713 8th Parkway', '887-692-2978');
insert into apprenant (id_appr, name, email, adress, phone) values (28, 'Nathanil Dunnet', 'ndunnetr@ca.gov', '86183 Vidon Center', '731-538-7817');
insert into apprenant (id_appr, name, email, adress, phone) values (29, 'Archy Twentyman', 'atwentymans@bloglovin.com', '54 Knutson Circle', '979-609-9953');
insert into apprenant (id_appr, name, email, adress, phone) values (30, 'Vince Haxby', 'vhaxbyt@friendfeed.com', '1575 2nd Drive', '806-817-9584');
insert into apprenant (id_appr, name, email, adress, phone) values (31, 'Penelopa Shine', 'pshineu@yellowbook.com', '740 Manley Center', '281-258-9765');
insert into apprenant (id_appr, name, email, adress, phone) values (32, 'Phillipe Mathet', 'pmathetv@thetimes.co.uk', '49615 Michigan Center', '916-842-6779');
insert into apprenant (id_appr, name, email, adress, phone) values (33, 'Bonny Wyrill', 'bwyrillw@springer.com', '6 Nelson Road', '849-952-9564');
insert into apprenant (id_appr, name, email, adress, phone) values (34, 'Tommie Dymoke', 'tdymokex@cornell.edu', '80 Fulton Terrace', '850-958-2838');
insert into apprenant (id_appr, name, email, adress, phone) values (35, 'Gilburt Fernant', 'gfernanty@joomla.org', '2 Sachs Place', '223-530-4981');
insert into apprenant (id_appr, name, email, adress, phone) values (36, 'Ruy Straun', 'rstraunz@usa.gov', '99279 Golf Junction', '878-963-1535');
insert into apprenant (id_appr, name, email, adress, phone) values (37, 'Etienne Queyos', 'equeyos10@blogs.com', '73576 8th Junction', '575-826-4736');
insert into apprenant (id_appr, name, email, adress, phone) values (38, 'Lottie Jeannet', 'ljeannet11@360.cn', '14759 Homewood Pass', '609-953-9984');
insert into apprenant (id_appr, name, email, adress, phone) values (39, 'Cherise Beyne', 'cbeyne12@discuz.net', '334 Leroy Street', '297-988-2706');
insert into apprenant (id_appr, name, email, adress, phone) values (40, 'Alayne Gathercoal', 'agathercoal13@shareasale.com', '24610 Brickson Park Road', '138-530-5324');
insert into apprenant (id_appr, name, email, adress, phone) values (41, 'Jeannette Quinnelly', 'jquinnelly14@twitpic.com', '30 Cottonwood Court', '149-627-6738');
insert into apprenant (id_appr, name, email, adress, phone) values (42, 'Sheilakathryn Wylam', 'swylam15@squidoo.com', '96 Fuller Place', '892-365-0361');
insert into apprenant (id_appr, name, email, adress, phone) values (43, 'Charlean Congrave', 'ccongrave16@sbwire.com', '209 Norway Maple Way', '971-397-6481');
insert into apprenant (id_appr, name, email, adress, phone) values (44, 'Kristoforo Parsonage', 'kparsonage17@walmart.com', '48674 Corry Circle', '701-326-4311');
insert into apprenant (id_appr, name, email, adress, phone) values (45, 'Zara Brussell', 'zbrussell18@time.com', '89700 Blaine Park', '818-328-0846');
insert into apprenant (id_appr, name, email, adress, phone) values (46, 'Corey MacDonough', 'cmacdonough19@amazon.com', '124 Esch Parkway', '237-291-0492');
insert into apprenant (id_appr, name, email, adress, phone) values (47, 'Halie Chad', 'hchad1a@xrea.com', '804 Pankratz Circle', '653-630-6042');
insert into apprenant (id_appr, name, email, adress, phone) values (48, 'Caryl McGrann', 'cmcgrann1b@bigcartel.com', '955 Brickson Park Street', '695-831-3146');
insert into apprenant (id_appr, name, email, adress, phone) values (49, 'Tamra Sherrock', 'tsherrock1c@ibm.com', '52999 Northland Lane', '166-706-4763');
insert into apprenant (id_appr, name, email, adress, phone) values (50, 'Linette Jovicevic', 'ljovicevic1d@theglobeandmail.com', '8 Merrick Plaza', '403-152-8258');

-- Insert rows materiel
insert into materiel (id, type, quantity, id_appr) values (1, 'vel', 26, 7);
insert into materiel (id, type, quantity, id_appr) values (2, 'nullam molestie nibh', 28, 4);
insert into materiel (id, type, quantity, id_appr) values (3, 'sapien cursus vestibulum proin', 49, 28);
insert into materiel (id, type, quantity, id_appr) values (4, 'nulla mollis molestie', 29, 39);
insert into materiel (id, type, quantity, id_appr) values (5, 'in felis eu sapien', 5, 26);
insert into materiel (id, type, quantity, id_appr) values (6, 'sed augue aliquam erat', 28, 44);
insert into materiel (id, type, quantity, id_appr) values (7, 'natoque', 40, 11);
insert into materiel (id, type, quantity, id_appr) values (8, 'blandit lacinia erat vestibulum', 30, 50);
insert into materiel (id, type, quantity, id_appr) values (9, 'eu felis fusce posuere', 45, 11);
insert into materiel (id, type, quantity, id_appr) values (10, 'ante vel', 40, 33);
insert into materiel (id, type, quantity, id_appr) values (11, 'mi in porttitor pede', 30, 26);
insert into materiel (id, type, quantity, id_appr) values (12, 'id luctus nec molestie sed', 22, 19);
insert into materiel (id, type, quantity, id_appr) values (13, 'tincidunt', 37, 26);
insert into materiel (id, type, quantity, id_appr) values (14, 'id', 7, 50);
insert into materiel (id, type, quantity, id_appr) values (15, 'potenti cras', 4, 11);
insert into materiel (id, type, quantity, id_appr) values (16, 'morbi quis tortor id', 16, 21);
insert into materiel (id, type, quantity, id_appr) values (17, 'ut volutpat sapien arcu sed', 6, 1);
insert into materiel (id, type, quantity, id_appr) values (18, 'lorem', 42, 21);
insert into materiel (id, type, quantity, id_appr) values (19, 'vitae', 11, 41);
insert into materiel (id, type, quantity, id_appr) values (20, 'sapien non mi integer ac', 35, 38);
insert into materiel (id, type, quantity, id_appr) values (21, 'sapien', 30, 19);
insert into materiel (id, type, quantity, id_appr) values (22, 'consequat dui nec nisi', 38, 33);
insert into materiel (id, type, quantity, id_appr) values (23, 'nulla quisque arcu libero', 24, 27);
insert into materiel (id, type, quantity, id_appr) values (24, 'ut', 24, 47);
insert into materiel (id, type, quantity, id_appr) values (25, 'eleifend', 4, 31);
insert into materiel (id, type, quantity, id_appr) values (26, 'orci pede venenatis non sodales', 13, 18);
insert into materiel (id, type, quantity, id_appr) values (27, 'nascetur ridiculus mus', 5, 17);
insert into materiel (id, type, quantity, id_appr) values (28, 'pellentesque viverra pede ac', 27, 31);
insert into materiel (id, type, quantity, id_appr) values (29, 'integer pede', 4, 30);
insert into materiel (id, type, quantity, id_appr) values (30, 'sit amet justo morbi', 43, 1);
insert into materiel (id, type, quantity, id_appr) values (31, 'lacinia eget tincidunt', 41, 9);
insert into materiel (id, type, quantity, id_appr) values (32, 'sed justo', 29, 32);
insert into materiel (id, type, quantity, id_appr) values (33, 'sem fusce consequat', 26, 50);
insert into materiel (id, type, quantity, id_appr) values (34, 'venenatis', 16, 35);
insert into materiel (id, type, quantity, id_appr) values (35, 'ante vivamus tortor duis mattis', 9, 43);
insert into materiel (id, type, quantity, id_appr) values (36, 'viverra eget congue eget semper', 17, 32);
insert into materiel (id, type, quantity, id_appr) values (37, 'vitae consectetuer', 36, 43);
insert into materiel (id, type, quantity, id_appr) values (38, 'in congue etiam justo', 17, 49);
insert into materiel (id, type, quantity, id_appr) values (39, 'nam', 48, 5);
insert into materiel (id, type, quantity, id_appr) values (40, 'semper est quam pharetra', 26, 39);
insert into materiel (id, type, quantity, id_appr) values (41, 'libero rutrum ac', 4, 36);
insert into materiel (id, type, quantity, id_appr) values (42, 'morbi ut odio cras', 25, 31);
insert into materiel (id, type, quantity, id_appr) values (43, 'nunc proin at turpis a', 45, 30);
insert into materiel (id, type, quantity, id_appr) values (44, 'orci pede', 8, 1);
insert into materiel (id, type, quantity, id_appr) values (45, 'faucibus', 14, 5);
insert into materiel (id, type, quantity, id_appr) values (46, 'felis donec semper', 49, 17);
insert into materiel (id, type, quantity, id_appr) values (47, 'varius ut blandit non interdum', 46, 11);
insert into materiel (id, type, quantity, id_appr) values (48, 'ultrices libero non mattis pulvinar', 33, 10);
insert into materiel (id, type, quantity, id_appr) values (49, 'et tempus semper est', 35, 12);
insert into materiel (id, type, quantity, id_appr) values (50, 'semper interdum', 24, 15);

-- Insert rows projet
insert into projet (id_projet, nom, date_debut, date_fin) values (1, 'justo', '2023-09-24', '2023-09-06');
insert into projet (id_projet, nom, date_debut, date_fin) values (2, 'odio odio elementum eu', '2023-02-20', '2023-11-29');
insert into projet (id_projet, nom, date_debut, date_fin) values (3, 'pellentesque quisque porta volutpat erat', '2023-10-11', '2023-08-22');
insert into projet (id_projet, nom, date_debut, date_fin) values (4, 'donec dapibus duis', '2023-03-15', '2023-02-19');
insert into projet (id_projet, nom, date_debut, date_fin) values (5, 'erat curabitur gravida', '2023-09-19', '2023-07-10');
insert into projet (id_projet, nom, date_debut, date_fin) values (6, 'nisi vulputate', '2023-03-27', '2023-05-24');
insert into projet (id_projet, nom, date_debut, date_fin) values (7, 'pellentesque', '2023-10-31', '2023-12-28');
insert into projet (id_projet, nom, date_debut, date_fin) values (8, 'faucibus', '2023-10-12', '2023-12-22');
insert into projet (id_projet, nom, date_debut, date_fin) values (9, 'erat quisque erat eros', '2023-11-16', '2024-01-01');
insert into projet (id_projet, nom, date_debut, date_fin) values (10, 'non', '2023-07-06', '2023-10-30');
insert into projet (id_projet, nom, date_debut, date_fin) values (11, 'consectetuer eget rutrum at lorem', '2023-10-15', '2023-02-03');
insert into projet (id_projet, nom, date_debut, date_fin) values (12, 'et', '2023-03-21', '2023-09-09');
insert into projet (id_projet, nom, date_debut, date_fin) values (13, 'morbi quis tortor', '2023-02-18', '2024-01-05');
insert into projet (id_projet, nom, date_debut, date_fin) values (14, 'diam in magna bibendum', '2024-01-17', '2023-11-27');
insert into projet (id_projet, nom, date_debut, date_fin) values (15, 'duis', '2023-05-11', '2023-06-07');
insert into projet (id_projet, nom, date_debut, date_fin) values (16, 'duis bibendum felis sed', '2023-12-01', '2023-02-16');
insert into projet (id_projet, nom, date_debut, date_fin) values (17, 'non ligula pellentesque ultrices', '2023-11-18', '2023-08-08');
insert into projet (id_projet, nom, date_debut, date_fin) values (18, 'praesent lectus vestibulum quam', '2023-11-01', '2023-04-02');
insert into projet (id_projet, nom, date_debut, date_fin) values (19, 'lorem ipsum dolor sit amet', '2023-12-04', '2023-02-19');
insert into projet (id_projet, nom, date_debut, date_fin) values (20, 'molestie sed', '2023-05-14', '2023-02-16');
insert into projet (id_projet, nom, date_debut, date_fin) values (21, 'cubilia curae donec pharetra magna', '2023-10-12', '2023-11-08');
insert into projet (id_projet, nom, date_debut, date_fin) values (22, 'nunc rhoncus dui vel sem', '2023-08-31', '2023-07-02');
insert into projet (id_projet, nom, date_debut, date_fin) values (23, 'congue eget semper rutrum nulla', '2023-12-13', '2023-01-24');
insert into projet (id_projet, nom, date_debut, date_fin) values (24, 'tristique', '2023-04-05', '2023-11-01');
insert into projet (id_projet, nom, date_debut, date_fin) values (25, 'nulla integer', '2023-08-05', '2023-04-23');
insert into projet (id_projet, nom, date_debut, date_fin) values (26, 'volutpat', '2023-06-19', '2023-05-25');
insert into projet (id_projet, nom, date_debut, date_fin) values (27, 'amet diam in magna bibendum', '2023-11-13', '2023-02-01');
insert into projet (id_projet, nom, date_debut, date_fin) values (28, 'eget nunc', '2023-07-29', '2023-10-26');
insert into projet (id_projet, nom, date_debut, date_fin) values (29, 'donec', '2023-10-25', '2023-11-28');
insert into projet (id_projet, nom, date_debut, date_fin) values (30, 'nibh in', '2023-06-06', '2023-03-06');
insert into projet (id_projet, nom, date_debut, date_fin) values (31, 'diam id ornare imperdiet', '2023-12-13', '2023-04-13');
insert into projet (id_projet, nom, date_debut, date_fin) values (32, 'accumsan', '2023-04-25', '2023-08-30');
insert into projet (id_projet, nom, date_debut, date_fin) values (33, 'erat fermentum justo nec condimentum', '2024-01-11', '2023-12-15');
insert into projet (id_projet, nom, date_debut, date_fin) values (34, 'eget vulputate', '2023-05-11', '2023-07-17');
insert into projet (id_projet, nom, date_debut, date_fin) values (35, 'at diam nam', '2023-09-06', '2023-03-09');
insert into projet (id_projet, nom, date_debut, date_fin) values (36, 'lacus curabitur at ipsum ac', '2023-09-30', '2024-01-10');
insert into projet (id_projet, nom, date_debut, date_fin) values (37, 'sit amet lobortis sapien sapien', '2023-09-03', '2023-12-05');
insert into projet (id_projet, nom, date_debut, date_fin) values (38, 'donec ut', '2023-04-18', '2023-07-04');
insert into projet (id_projet, nom, date_debut, date_fin) values (39, 'eget massa tempor convallis', '2023-08-01', '2023-11-17');
insert into projet (id_projet, nom, date_debut, date_fin) values (40, 'leo rhoncus sed vestibulum', '2023-03-29', '2023-11-07');
insert into projet (id_projet, nom, date_debut, date_fin) values (41, 'erat eros viverra eget congue', '2023-03-14', '2023-09-13');
insert into projet (id_projet, nom, date_debut, date_fin) values (42, 'orci luctus et ultrices posuere', '2023-04-12', '2023-12-27');
insert into projet (id_projet, nom, date_debut, date_fin) values (43, 'lobortis', '2023-08-12', '2023-07-07');
insert into projet (id_projet, nom, date_debut, date_fin) values (44, 'augue vestibulum', '2023-08-15', '2023-12-17');
insert into projet (id_projet, nom, date_debut, date_fin) values (45, 'sapien cum sociis', '2023-06-18', '2023-01-31');
insert into projet (id_projet, nom, date_debut, date_fin) values (46, 'consectetuer eget', '2023-04-10', '2023-09-13');
insert into projet (id_projet, nom, date_debut, date_fin) values (47, 'quisque erat eros', '2023-03-02', '2023-09-26');
insert into projet (id_projet, nom, date_debut, date_fin) values (48, 'ultrices erat', '2023-01-30', '2024-01-09');
insert into projet (id_projet, nom, date_debut, date_fin) values (49, 'lorem ipsum dolor', '2023-09-16', '2023-05-08');
insert into projet (id_projet, nom, date_debut, date_fin) values (50, 'ac diam cras pellentesque', '2023-09-09', '2023-09-07');

-- Insert rows groupe
insert into groupe (id_group, nom, id_projet) values (1, 'consequat metus sapien', 12);
insert into groupe (id_group, nom, id_projet) values (2, 'ante ipsum primis', 42);
insert into groupe (id_group, nom, id_projet) values (3, 'pede', 13);
insert into groupe (id_group, nom, id_projet) values (4, 'mauris sit amet', 33);
insert into groupe (id_group, nom, id_projet) values (5, 'parturient montes nascetur', 49);
insert into groupe (id_group, nom, id_projet) values (6, 'mauris', 31);
insert into groupe (id_group, nom, id_projet) values (7, 'eu mi', 4);
insert into groupe (id_group, nom, id_projet) values (8, 'est congue elementum in hac', 10);
insert into groupe (id_group, nom, id_projet) values (9, 'proin at turpis', 26);
insert into groupe (id_group, nom, id_projet) values (10, 'ultrices vel augue vestibulum', 13);
insert into groupe (id_group, nom, id_projet) values (11, 'dui proin', 30);
insert into groupe (id_group, nom, id_projet) values (12, 'vitae consectetuer eget rutrum', 2);
insert into groupe (id_group, nom, id_projet) values (13, 'iaculis', 41);
insert into groupe (id_group, nom, id_projet) values (14, 'rhoncus', 46);
insert into groupe (id_group, nom, id_projet) values (15, 'sapien in sapien', 3);
insert into groupe (id_group, nom, id_projet) values (16, 'lobortis est', 11);
insert into groupe (id_group, nom, id_projet) values (17, 'ipsum praesent blandit lacinia erat', 19);
insert into groupe (id_group, nom, id_projet) values (18, 'luctus nec molestie sed justo', 45);
insert into groupe (id_group, nom, id_projet) values (19, 'amet turpis elementum ligula', 17);
insert into groupe (id_group, nom, id_projet) values (20, 'eu interdum eu tincidunt in', 49);
insert into groupe (id_group, nom, id_projet) values (21, 'morbi a ipsum integer a', 10);
insert into groupe (id_group, nom, id_projet) values (22, 'mi', 39);
insert into groupe (id_group, nom, id_projet) values (23, 'nec sem duis aliquam convallis', 2);
insert into groupe (id_group, nom, id_projet) values (24, 'in felis', 14);
insert into groupe (id_group, nom, id_projet) values (25, 'eget nunc donec', 22);
insert into groupe (id_group, nom, id_projet) values (26, 'donec', 29);
insert into groupe (id_group, nom, id_projet) values (27, 'quam pede lobortis ligula sit', 48);
insert into groupe (id_group, nom, id_projet) values (28, 'luctus', 7);
insert into groupe (id_group, nom, id_projet) values (29, 'in eleifend quam a odio', 48);
insert into groupe (id_group, nom, id_projet) values (30, 'eu nibh quisque id', 46);
insert into groupe (id_group, nom, id_projet) values (31, 'mauris eget massa tempor convallis', 20);
insert into groupe (id_group, nom, id_projet) values (32, 'non ligula pellentesque ultrices phasellus', 21);
insert into groupe (id_group, nom, id_projet) values (33, 'eu est congue elementum in', 13);
insert into groupe (id_group, nom, id_projet) values (34, 'elementum in hac', 35);
insert into groupe (id_group, nom, id_projet) values (35, 'cum sociis natoque penatibus et', 15);
insert into groupe (id_group, nom, id_projet) values (36, 'diam in magna bibendum', 47);
insert into groupe (id_group, nom, id_projet) values (37, 'consectetuer adipiscing elit proin risus', 18);
insert into groupe (id_group, nom, id_projet) values (38, 'tristique', 9);
insert into groupe (id_group, nom, id_projet) values (39, 'habitasse platea dictumst etiam faucibus', 48);
insert into groupe (id_group, nom, id_projet) values (40, 'sem mauris laoreet ut rhoncus', 43);
insert into groupe (id_group, nom, id_projet) values (41, 'at nunc commodo placerat praesent', 4);
insert into groupe (id_group, nom, id_projet) values (42, 'sem praesent id massa id', 35);
insert into groupe (id_group, nom, id_projet) values (43, 'ipsum aliquam non mauris', 36);
insert into groupe (id_group, nom, id_projet) values (44, 'accumsan tortor quis turpis', 18);
insert into groupe (id_group, nom, id_projet) values (45, 'aenean lectus pellentesque', 50);
insert into groupe (id_group, nom, id_projet) values (46, 'enim leo rhoncus sed', 3);
insert into groupe (id_group, nom, id_projet) values (47, 'aliquet', 1);
insert into groupe (id_group, nom, id_projet) values (48, 'nulla neque', 7);
insert into groupe (id_group, nom, id_projet) values (49, 'mi nulla ac enim', 41);
insert into groupe (id_group, nom, id_projet) values (50, 'turpis donec posuere metus vitae', 42);

-- Insert appr_group
insert into appr_group (id_appr, id_group) values (22, 16);
insert into appr_group (id_appr, id_group) values (27, 49);
insert into appr_group (id_appr, id_group) values (47, 32);
insert into appr_group (id_appr, id_group) values (5, 11);
insert into appr_group (id_appr, id_group) values (40, 1);
insert into appr_group (id_appr, id_group) values (38, 15);
insert into appr_group (id_appr, id_group) values (46, 19);
insert into appr_group (id_appr, id_group) values (33, 17);
insert into appr_group (id_appr, id_group) values (33, 15);
insert into appr_group (id_appr, id_group) values (49, 38);
insert into appr_group (id_appr, id_group) values (2, 18);
insert into appr_group (id_appr, id_group) values (4, 38);
insert into appr_group (id_appr, id_group) values (34, 31);
insert into appr_group (id_appr, id_group) values (2, 5);
insert into appr_group (id_appr, id_group) values (15, 33);
insert into appr_group (id_appr, id_group) values (50, 31);
insert into appr_group (id_appr, id_group) values (30, 12);
insert into appr_group (id_appr, id_group) values (16, 12);
insert into appr_group (id_appr, id_group) values (32, 14);
insert into appr_group (id_appr, id_group) values (47, 10);
insert into appr_group (id_appr, id_group) values (38, 15);
insert into appr_group (id_appr, id_group) values (9, 11);
insert into appr_group (id_appr, id_group) values (42, 4);
insert into appr_group (id_appr, id_group) values (3, 32);
insert into appr_group (id_appr, id_group) values (10, 24);
insert into appr_group (id_appr, id_group) values (30, 11);
insert into appr_group (id_appr, id_group) values (24, 3);
insert into appr_group (id_appr, id_group) values (41, 45);
insert into appr_group (id_appr, id_group) values (26, 3);
insert into appr_group (id_appr, id_group) values (49, 36);
insert into appr_group (id_appr, id_group) values (43, 30);
insert into appr_group (id_appr, id_group) values (49, 42);
insert into appr_group (id_appr, id_group) values (31, 34);
insert into appr_group (id_appr, id_group) values (13, 19);
insert into appr_group (id_appr, id_group) values (46, 1);
insert into appr_group (id_appr, id_group) values (24, 39);
insert into appr_group (id_appr, id_group) values (1, 22);
insert into appr_group (id_appr, id_group) values (18, 19);
insert into appr_group (id_appr, id_group) values (33, 38);
insert into appr_group (id_appr, id_group) values (30, 22);
insert into appr_group (id_appr, id_group) values (33, 7);
insert into appr_group (id_appr, id_group) values (41, 18);
insert into appr_group (id_appr, id_group) values (20, 42);
insert into appr_group (id_appr, id_group) values (31, 32);
insert into appr_group (id_appr, id_group) values (33, 21);
insert into appr_group (id_appr, id_group) values (32, 49);
insert into appr_group (id_appr, id_group) values (13, 7);
insert into appr_group (id_appr, id_group) values (24, 31);
insert into appr_group (id_appr, id_group) values (45, 50);
insert into appr_group (id_appr, id_group) values (31, 1);

-- Insert rows competences
insert into competences (id_comp, nom, acquises) values (1, 'eget tincidunt eget', true);
insert into competences (id_comp, nom, acquises) values (2, 'in est risus auctor sed', true);
insert into competences (id_comp, nom, acquises) values (3, 'volutpat', false);
insert into competences (id_comp, nom, acquises) values (4, 'at feugiat non pretium quis', true);
insert into competences (id_comp, nom, acquises) values (5, 'id consequat in consequat ut', false);
insert into competences (id_comp, nom, acquises) values (6, 'mauris morbi', false);
insert into competences (id_comp, nom, acquises) values (7, 'quam sapien', true);
insert into competences (id_comp, nom, acquises) values (8, 'amet lobortis sapien sapien', false);
insert into competences (id_comp, nom, acquises) values (9, 'pretium', false);
insert into competences (id_comp, nom, acquises) values (10, 'in imperdiet et commodo', true);
insert into competences (id_comp, nom, acquises) values (11, 'felis', false);
insert into competences (id_comp, nom, acquises) values (12, 'primis in faucibus', true);
insert into competences (id_comp, nom, acquises) values (13, 'feugiat et eros vestibulum ac', true);
insert into competences (id_comp, nom, acquises) values (14, 'aenean', true);
insert into competences (id_comp, nom, acquises) values (15, 'quis odio consequat', false);
insert into competences (id_comp, nom, acquises) values (16, 'auctor gravida sem praesent id', true);
insert into competences (id_comp, nom, acquises) values (17, 'sit', true);
insert into competences (id_comp, nom, acquises) values (18, 'magna vulputate', false);
insert into competences (id_comp, nom, acquises) values (19, 'at nulla', false);
insert into competences (id_comp, nom, acquises) values (20, 'eu magna vulputate luctus', true);
insert into competences (id_comp, nom, acquises) values (21, 'lorem', false);
insert into competences (id_comp, nom, acquises) values (22, 'egestas', false);
insert into competences (id_comp, nom, acquises) values (23, 'porttitor pede', false);
insert into competences (id_comp, nom, acquises) values (24, 'a', false);
insert into competences (id_comp, nom, acquises) values (25, 'posuere cubilia', false);
insert into competences (id_comp, nom, acquises) values (26, 'maecenas rhoncus aliquam', true);
insert into competences (id_comp, nom, acquises) values (27, 'dictumst', false);
insert into competences (id_comp, nom, acquises) values (28, 'pellentesque ultrices mattis odio', true);
insert into competences (id_comp, nom, acquises) values (29, 'nam', false);
insert into competences (id_comp, nom, acquises) values (30, 'est quam', true);
insert into competences (id_comp, nom, acquises) values (31, 'ut', false);
insert into competences (id_comp, nom, acquises) values (32, 'ac leo pellentesque', true);
insert into competences (id_comp, nom, acquises) values (33, 'sapien non mi', false);
insert into competences (id_comp, nom, acquises) values (34, 'maecenas pulvinar', false);
insert into competences (id_comp, nom, acquises) values (35, 'mus vivamus vestibulum sagittis', true);
insert into competences (id_comp, nom, acquises) values (36, 'nonummy integer non velit donec', true);
insert into competences (id_comp, nom, acquises) values (37, 'morbi vel lectus in', false);
insert into competences (id_comp, nom, acquises) values (38, 'pede', true);
insert into competences (id_comp, nom, acquises) values (39, 'faucibus', false);
insert into competences (id_comp, nom, acquises) values (40, 'sed vestibulum sit amet cursus', true);
insert into competences (id_comp, nom, acquises) values (41, 'vestibulum eget', false);
insert into competences (id_comp, nom, acquises) values (42, 'sit amet consectetuer adipiscing elit', false);
insert into competences (id_comp, nom, acquises) values (43, 'varius nulla facilisi cras', false);
insert into competences (id_comp, nom, acquises) values (44, 'nec sem duis aliquam', false);
insert into competences (id_comp, nom, acquises) values (45, 'ut mauris', true);
insert into competences (id_comp, nom, acquises) values (46, 'pellentesque eget', true);
insert into competences (id_comp, nom, acquises) values (47, 'ipsum primis in faucibus orci', false);
insert into competences (id_comp, nom, acquises) values (48, 'sapien iaculis congue', false);
insert into competences (id_comp, nom, acquises) values (49, 'varius ut blandit non interdum', false);
insert into competences (id_comp, nom, acquises) values (50, 'praesent', true);

-- Insert raws comp_projet
insert into comp_projet (id_comp, id_projet) values (16, 13);
insert into comp_projet (id_comp, id_projet) values (37, 21);
insert into comp_projet (id_comp, id_projet) values (5, 33);
insert into comp_projet (id_comp, id_projet) values (18, 8);
insert into comp_projet (id_comp, id_projet) values (20, 49);
insert into comp_projet (id_comp, id_projet) values (8, 35);
insert into comp_projet (id_comp, id_projet) values (30, 19);
insert into comp_projet (id_comp, id_projet) values (11, 47);
insert into comp_projet (id_comp, id_projet) values (25, 23);
insert into comp_projet (id_comp, id_projet) values (8, 30);
insert into comp_projet (id_comp, id_projet) values (36, 15);
insert into comp_projet (id_comp, id_projet) values (30, 11);
insert into comp_projet (id_comp, id_projet) values (40, 39);
insert into comp_projet (id_comp, id_projet) values (40, 13);
insert into comp_projet (id_comp, id_projet) values (34, 8);
insert into comp_projet (id_comp, id_projet) values (11, 28);
insert into comp_projet (id_comp, id_projet) values (35, 15);
insert into comp_projet (id_comp, id_projet) values (40, 38);
insert into comp_projet (id_comp, id_projet) values (17, 26);
insert into comp_projet (id_comp, id_projet) values (13, 25);
insert into comp_projet (id_comp, id_projet) values (37, 23);
insert into comp_projet (id_comp, id_projet) values (14, 16);
insert into comp_projet (id_comp, id_projet) values (5, 15);
insert into comp_projet (id_comp, id_projet) values (33, 29);
insert into comp_projet (id_comp, id_projet) values (31, 7);
insert into comp_projet (id_comp, id_projet) values (7, 19);
insert into comp_projet (id_comp, id_projet) values (35, 13);
insert into comp_projet (id_comp, id_projet) values (27, 33);
insert into comp_projet (id_comp, id_projet) values (7, 21);
insert into comp_projet (id_comp, id_projet) values (21, 49);
insert into comp_projet (id_comp, id_projet) values (26, 42);
insert into comp_projet (id_comp, id_projet) values (12, 47);
insert into comp_projet (id_comp, id_projet) values (19, 21);
insert into comp_projet (id_comp, id_projet) values (13, 5);
insert into comp_projet (id_comp, id_projet) values (24, 43);
insert into comp_projet (id_comp, id_projet) values (22, 12);
insert into comp_projet (id_comp, id_projet) values (3, 37);
insert into comp_projet (id_comp, id_projet) values (20, 18);
insert into comp_projet (id_comp, id_projet) values (28, 29);
insert into comp_projet (id_comp, id_projet) values (6, 47);
insert into comp_projet (id_comp, id_projet) values (21, 3);
insert into comp_projet (id_comp, id_projet) values (33, 48);
insert into comp_projet (id_comp, id_projet) values (19, 20);
insert into comp_projet (id_comp, id_projet) values (17, 37);
insert into comp_projet (id_comp, id_projet) values (33, 8);
insert into comp_projet (id_comp, id_projet) values (49, 9);
insert into comp_projet (id_comp, id_projet) values (11, 2);
insert into comp_projet (id_comp, id_projet) values (16, 1);
insert into comp_projet (id_comp, id_projet) values (41, 8);
insert into comp_projet (id_comp, id_projet) values (3, 1);

-- Insert rows presence
insert into presence (id_pres, signature, date_debut, date_fin) values (1, 'est', '2023-07-27', '2023-08-07');
insert into presence (id_pres, signature, date_debut, date_fin) values (2, 'maecenas rhoncus aliquam', '2023-09-02', '2023-01-26');
insert into presence (id_pres, signature, date_debut, date_fin) values (3, 'vestibulum proin eu', '2023-05-03', '2023-04-26');
insert into presence (id_pres, signature, date_debut, date_fin) values (4, 'mauris viverra diam vitae quam', '2023-09-26', '2023-05-24');
insert into presence (id_pres, signature, date_debut, date_fin) values (5, 'molestie lorem quisque', '2023-07-11', '2023-06-10');
insert into presence (id_pres, signature, date_debut, date_fin) values (6, 'donec', '2023-05-09', '2023-11-16');
insert into presence (id_pres, signature, date_debut, date_fin) values (7, 'leo odio porttitor id', '2023-06-09', '2023-05-02');
insert into presence (id_pres, signature, date_debut, date_fin) values (8, 'volutpat sapien arcu', '2023-03-15', '2023-11-16');
insert into presence (id_pres, signature, date_debut, date_fin) values (9, 'tortor sollicitudin mi', '2023-12-07', '2023-05-04');
insert into presence (id_pres, signature, date_debut, date_fin) values (10, 'ligula in lacus curabitur', '2023-11-01', '2024-01-11');
insert into presence (id_pres, signature, date_debut, date_fin) values (11, 'eu', '2023-12-13', '2024-01-18');
insert into presence (id_pres, signature, date_debut, date_fin) values (12, 'sagittis sapien', '2023-06-26', '2023-04-27');
insert into presence (id_pres, signature, date_debut, date_fin) values (13, 'justo aliquam quis', '2023-02-03', '2023-12-12');
insert into presence (id_pres, signature, date_debut, date_fin) values (14, 'platea dictumst', '2023-07-19', '2023-09-30');
insert into presence (id_pres, signature, date_debut, date_fin) values (15, 'consequat', '2023-05-30', '2023-04-16');
insert into presence (id_pres, signature, date_debut, date_fin) values (16, 'nonummy', '2023-12-16', '2023-01-23');
insert into presence (id_pres, signature, date_debut, date_fin) values (17, 'nisl', '2023-05-26', '2023-07-20');
insert into presence (id_pres, signature, date_debut, date_fin) values (18, 'etiam vel augue vestibulum rutrum', '2023-05-03', '2023-04-22');
insert into presence (id_pres, signature, date_debut, date_fin) values (19, 'a ipsum integer', '2023-09-24', '2023-09-23');
insert into presence (id_pres, signature, date_debut, date_fin) values (20, 'felis', '2023-04-13', '2023-09-20');
insert into presence (id_pres, signature, date_debut, date_fin) values (21, 'morbi odio odio elementum', '2023-07-28', '2023-08-20');
insert into presence (id_pres, signature, date_debut, date_fin) values (22, 'quisque id justo sit', '2023-03-30', '2023-06-24');
insert into presence (id_pres, signature, date_debut, date_fin) values (23, 'phasellus in felis', '2023-08-19', '2023-12-27');
insert into presence (id_pres, signature, date_debut, date_fin) values (24, 'penatibus et magnis dis parturient', '2023-10-09', '2023-11-03');
insert into presence (id_pres, signature, date_debut, date_fin) values (25, 'eu interdum eu', '2023-08-20', '2023-07-08');
insert into presence (id_pres, signature, date_debut, date_fin) values (26, 'ac', '2023-10-24', '2023-12-11');
insert into presence (id_pres, signature, date_debut, date_fin) values (27, 'commodo placerat praesent blandit', '2023-12-26', '2024-01-22');
insert into presence (id_pres, signature, date_debut, date_fin) values (28, 'nisl', '2023-11-07', '2023-08-28');
insert into presence (id_pres, signature, date_debut, date_fin) values (29, 'enim', '2023-05-19', '2023-01-25');
insert into presence (id_pres, signature, date_debut, date_fin) values (30, 'in blandit ultrices enim lorem', '2023-08-13', '2023-05-29');
insert into presence (id_pres, signature, date_debut, date_fin) values (31, 'auctor sed tristique', '2023-08-19', '2023-09-22');
insert into presence (id_pres, signature, date_debut, date_fin) values (32, 'in', '2023-06-29', '2023-05-13');
insert into presence (id_pres, signature, date_debut, date_fin) values (33, 'risus semper porta volutpat', '2023-09-04', '2023-12-31');
insert into presence (id_pres, signature, date_debut, date_fin) values (34, 'pellentesque ultrices', '2023-05-22', '2023-07-08');
insert into presence (id_pres, signature, date_debut, date_fin) values (35, 'nec dui luctus rutrum', '2023-06-30', '2023-02-09');
insert into presence (id_pres, signature, date_debut, date_fin) values (36, 'mi nulla ac', '2023-04-16', '2023-11-06');
insert into presence (id_pres, signature, date_debut, date_fin) values (37, 'lacus morbi', '2023-08-05', '2023-05-28');
insert into presence (id_pres, signature, date_debut, date_fin) values (38, 'sit amet justo morbi', '2023-02-24', '2023-02-09');
insert into presence (id_pres, signature, date_debut, date_fin) values (39, 'est quam pharetra magna ac', '2023-10-28', '2023-03-22');
insert into presence (id_pres, signature, date_debut, date_fin) values (40, 'porttitor', '2023-11-17', '2023-05-29');
insert into presence (id_pres, signature, date_debut, date_fin) values (41, 'cubilia curae', '2023-10-18', '2023-11-21');
insert into presence (id_pres, signature, date_debut, date_fin) values (42, 'consectetuer adipiscing elit', '2023-07-19', '2023-11-13');
insert into presence (id_pres, signature, date_debut, date_fin) values (43, 'magna bibendum imperdiet nullam orci', '2023-11-03', '2023-11-26');
insert into presence (id_pres, signature, date_debut, date_fin) values (44, 'feugiat et eros', '2023-04-09', '2023-06-11');
insert into presence (id_pres, signature, date_debut, date_fin) values (45, 'aliquam lacus morbi', '2023-07-30', '2023-11-08');
insert into presence (id_pres, signature, date_debut, date_fin) values (46, 'in hac habitasse platea dictumst', '2023-10-01', '2023-09-21');
insert into presence (id_pres, signature, date_debut, date_fin) values (47, 'aliquet', '2023-06-26', '2023-09-08');
insert into presence (id_pres, signature, date_debut, date_fin) values (48, 'quis', '2023-11-10', '2023-09-20');
insert into presence (id_pres, signature, date_debut, date_fin) values (49, 'eleifend', '2023-02-13', '2023-02-12');
insert into presence (id_pres, signature, date_debut, date_fin) values (50, 'tortor duis mattis', '2023-10-02', '2023-04-01');

-- Insert rows appr_pres
insert into appr_pres (id_appr, id_pres) values (29, 26);
insert into appr_pres (id_appr, id_pres) values (24, 33);
insert into appr_pres (id_appr, id_pres) values (6, 36);
insert into appr_pres (id_appr, id_pres) values (47, 31);
insert into appr_pres (id_appr, id_pres) values (12, 16);
insert into appr_pres (id_appr, id_pres) values (40, 28);
insert into appr_pres (id_appr, id_pres) values (22, 40);
insert into appr_pres (id_appr, id_pres) values (35, 1);
insert into appr_pres (id_appr, id_pres) values (26, 4);
insert into appr_pres (id_appr, id_pres) values (49, 5);
insert into appr_pres (id_appr, id_pres) values (35, 18);
insert into appr_pres (id_appr, id_pres) values (45, 14);
insert into appr_pres (id_appr, id_pres) values (35, 42);
insert into appr_pres (id_appr, id_pres) values (4, 26);
insert into appr_pres (id_appr, id_pres) values (21, 13);
insert into appr_pres (id_appr, id_pres) values (19, 16);
insert into appr_pres (id_appr, id_pres) values (14, 3);
insert into appr_pres (id_appr, id_pres) values (36, 9);
insert into appr_pres (id_appr, id_pres) values (50, 25);
insert into appr_pres (id_appr, id_pres) values (5, 32);
insert into appr_pres (id_appr, id_pres) values (7, 7);
insert into appr_pres (id_appr, id_pres) values (16, 23);
insert into appr_pres (id_appr, id_pres) values (23, 32);
insert into appr_pres (id_appr, id_pres) values (18, 12);
insert into appr_pres (id_appr, id_pres) values (8, 44);
insert into appr_pres (id_appr, id_pres) values (37, 31);
insert into appr_pres (id_appr, id_pres) values (13, 7);
insert into appr_pres (id_appr, id_pres) values (30, 34);
insert into appr_pres (id_appr, id_pres) values (41, 30);
insert into appr_pres (id_appr, id_pres) values (17, 28);
insert into appr_pres (id_appr, id_pres) values (33, 27);
insert into appr_pres (id_appr, id_pres) values (49, 33);
insert into appr_pres (id_appr, id_pres) values (45, 13);
insert into appr_pres (id_appr, id_pres) values (4, 31);
insert into appr_pres (id_appr, id_pres) values (15, 11);
insert into appr_pres (id_appr, id_pres) values (27, 42);
insert into appr_pres (id_appr, id_pres) values (48, 11);
insert into appr_pres (id_appr, id_pres) values (6, 23);
insert into appr_pres (id_appr, id_pres) values (46, 48);
insert into appr_pres (id_appr, id_pres) values (25, 33);
insert into appr_pres (id_appr, id_pres) values (38, 35);
insert into appr_pres (id_appr, id_pres) values (19, 7);
insert into appr_pres (id_appr, id_pres) values (37, 1);
insert into appr_pres (id_appr, id_pres) values (13, 26);
insert into appr_pres (id_appr, id_pres) values (36, 33);
insert into appr_pres (id_appr, id_pres) values (24, 35);
insert into appr_pres (id_appr, id_pres) values (18, 42);
insert into appr_pres (id_appr, id_pres) values (45, 27);
insert into appr_pres (id_appr, id_pres) values (24, 8);
insert into appr_pres (id_appr, id_pres) values (8, 26);

-- Insert rows diplomes
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (1, 'proin interdum mauris', '2023-06-06', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (2, 'eget elit sodales', '2023-06-07', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (3, 'justo lacinia', '2023-10-19', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (4, 'vehicula', '2023-11-14', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (5, 'blandit', '2023-06-27', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (6, 'nam ultrices libero non mattis', '2023-09-19', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (7, 'dolor morbi vel lectus in', '2023-10-04', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (8, 'luctus et', '2023-06-21', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (9, 'augue', '2023-03-24', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (10, 'donec ut mauris', '2023-05-12', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (11, 'nisl nunc', '2023-05-10', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (12, 'justo sit', '2023-06-04', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (13, 'orci luctus et', '2024-01-15', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (14, 'sollicitudin mi sit amet lobortis', '2023-05-18', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (15, 'quam sollicitudin', '2023-06-03', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (16, 'nunc vestibulum', '2023-03-17', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (17, 'lectus in quam fringilla rhoncus', '2024-01-08', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (18, 'sed ante vivamus tortor', '2023-05-07', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (19, 'ut volutpat sapien', '2023-01-24', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (20, 'eu sapien', '2023-12-21', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (21, 'augue vestibulum rutrum rutrum neque', '2023-04-05', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (22, 'luctus', '2023-01-29', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (23, 'amet sapien dignissim', '2023-11-28', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (24, 'pede lobortis ligula sit amet', '2023-11-27', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (25, 'justo sollicitudin', '2023-03-24', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (26, 'lorem integer tincidunt ante', '2023-10-29', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (27, 'cursus vestibulum', '2023-09-06', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (28, 'amet consectetuer adipiscing', '2023-08-09', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (29, 'erat curabitur gravida nisi at', '2023-08-20', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (30, 'purus phasellus in', '2023-06-28', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (31, 'sapien', '2023-06-10', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (32, 'nulla', '2023-07-04', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (33, 'libero nam dui proin leo', '2023-12-28', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (34, 'amet consectetuer', '2023-02-16', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (35, 'tempus semper', '2023-07-26', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (36, 'id luctus nec molestie sed', '2023-11-19', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (37, 'rutrum rutrum neque aenean', '2023-10-03', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (38, 'tincidunt', '2023-07-23', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (39, 'quam turpis adipiscing', '2023-02-26', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (40, 'quam pede lobortis ligula', '2023-09-17', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (41, 'amet sapien dignissim', '2023-10-18', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (42, 'sed nisl nunc rhoncus', '2023-04-18', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (43, 'convallis morbi odio odio', '2024-01-11', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (44, 'nulla suspendisse potenti', '2024-01-07', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (45, 'sem duis aliquam', '2023-02-02', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (46, 'amet turpis', '2023-10-23', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (47, 'bibendum felis', '2023-09-25', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (48, 'diam id ornare imperdiet', '2023-08-10', false);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (49, 'cum sociis natoque penatibus et', '2023-06-29', true);
insert into diplomes (id_diplome, nom, date_obtention, acquis) values (50, 'luctus cum sociis natoque penatibus', '2023-07-16', false);

-- Insert rows appr_dip
insert into appr_dip (id_appr, id_diplome) values (11, 27);
insert into appr_dip (id_appr, id_diplome) values (38, 48);
insert into appr_dip (id_appr, id_diplome) values (48, 37);
insert into appr_dip (id_appr, id_diplome) values (47, 18);
insert into appr_dip (id_appr, id_diplome) values (29, 50);
insert into appr_dip (id_appr, id_diplome) values (21, 34);
insert into appr_dip (id_appr, id_diplome) values (22, 3);
insert into appr_dip (id_appr, id_diplome) values (43, 21);
insert into appr_dip (id_appr, id_diplome) values (31, 22);
insert into appr_dip (id_appr, id_diplome) values (19, 5);
insert into appr_dip (id_appr, id_diplome) values (38, 42);
insert into appr_dip (id_appr, id_diplome) values (34, 1);
insert into appr_dip (id_appr, id_diplome) values (31, 14);
insert into appr_dip (id_appr, id_diplome) values (7, 14);
insert into appr_dip (id_appr, id_diplome) values (19, 35);
insert into appr_dip (id_appr, id_diplome) values (45, 24);
insert into appr_dip (id_appr, id_diplome) values (1, 48);
insert into appr_dip (id_appr, id_diplome) values (41, 37);
insert into appr_dip (id_appr, id_diplome) values (36, 6);
insert into appr_dip (id_appr, id_diplome) values (45, 49);
insert into appr_dip (id_appr, id_diplome) values (12, 5);
insert into appr_dip (id_appr, id_diplome) values (3, 21);
insert into appr_dip (id_appr, id_diplome) values (22, 26);
insert into appr_dip (id_appr, id_diplome) values (41, 39);
insert into appr_dip (id_appr, id_diplome) values (5, 15);
insert into appr_dip (id_appr, id_diplome) values (33, 23);
insert into appr_dip (id_appr, id_diplome) values (22, 2);
insert into appr_dip (id_appr, id_diplome) values (23, 10);
insert into appr_dip (id_appr, id_diplome) values (14, 3);
insert into appr_dip (id_appr, id_diplome) values (28, 49);
insert into appr_dip (id_appr, id_diplome) values (24, 23);
insert into appr_dip (id_appr, id_diplome) values (49, 11);
insert into appr_dip (id_appr, id_diplome) values (6, 39);
insert into appr_dip (id_appr, id_diplome) values (7, 39);
insert into appr_dip (id_appr, id_diplome) values (20, 19);
insert into appr_dip (id_appr, id_diplome) values (37, 36);
insert into appr_dip (id_appr, id_diplome) values (30, 48);
insert into appr_dip (id_appr, id_diplome) values (11, 47);
insert into appr_dip (id_appr, id_diplome) values (46, 10);
insert into appr_dip (id_appr, id_diplome) values (2, 33);
insert into appr_dip (id_appr, id_diplome) values (7, 23);
insert into appr_dip (id_appr, id_diplome) values (28, 18);
insert into appr_dip (id_appr, id_diplome) values (8, 8);
insert into appr_dip (id_appr, id_diplome) values (13, 43);
insert into appr_dip (id_appr, id_diplome) values (50, 9);
insert into appr_dip (id_appr, id_diplome) values (36, 17);
insert into appr_dip (id_appr, id_diplome) values (26, 34);
insert into appr_dip (id_appr, id_diplome) values (30, 12);
insert into appr_dip (id_appr, id_diplome) values (31, 30);
insert into appr_dip (id_appr, id_diplome) values (36, 14);

-- Insert des rows appr_comp
insert into appr_comp (id_appr, id_comp) values (25, 39);
insert into appr_comp (id_appr, id_comp) values (46, 27);
insert into appr_comp (id_appr, id_comp) values (45, 9);
insert into appr_comp (id_appr, id_comp) values (12, 28);
insert into appr_comp (id_appr, id_comp) values (22, 8);
insert into appr_comp (id_appr, id_comp) values (17, 43);
insert into appr_comp (id_appr, id_comp) values (20, 44);
insert into appr_comp (id_appr, id_comp) values (20, 39);
insert into appr_comp (id_appr, id_comp) values (40, 20);
insert into appr_comp (id_appr, id_comp) values (39, 40);
insert into appr_comp (id_appr, id_comp) values (11, 8);
insert into appr_comp (id_appr, id_comp) values (41, 39);
insert into appr_comp (id_appr, id_comp) values (21, 29);
insert into appr_comp (id_appr, id_comp) values (35, 6);
insert into appr_comp (id_appr, id_comp) values (47, 33);
insert into appr_comp (id_appr, id_comp) values (5, 24);
insert into appr_comp (id_appr, id_comp) values (12, 29);
insert into appr_comp (id_appr, id_comp) values (44, 11);
insert into appr_comp (id_appr, id_comp) values (35, 29);
insert into appr_comp (id_appr, id_comp) values (49, 4);
insert into appr_comp (id_appr, id_comp) values (26, 42);
insert into appr_comp (id_appr, id_comp) values (4, 20);
insert into appr_comp (id_appr, id_comp) values (49, 12);
insert into appr_comp (id_appr, id_comp) values (37, 27);
insert into appr_comp (id_appr, id_comp) values (7, 24);
insert into appr_comp (id_appr, id_comp) values (32, 31);
insert into appr_comp (id_appr, id_comp) values (11, 13);
insert into appr_comp (id_appr, id_comp) values (34, 8);
insert into appr_comp (id_appr, id_comp) values (26, 27);
insert into appr_comp (id_appr, id_comp) values (16, 40);
insert into appr_comp (id_appr, id_comp) values (49, 6);
insert into appr_comp (id_appr, id_comp) values (48, 26);
insert into appr_comp (id_appr, id_comp) values (27, 1);
insert into appr_comp (id_appr, id_comp) values (31, 48);
insert into appr_comp (id_appr, id_comp) values (12, 2);
insert into appr_comp (id_appr, id_comp) values (29, 43);
insert into appr_comp (id_appr, id_comp) values (10, 33);
insert into appr_comp (id_appr, id_comp) values (26, 35);
insert into appr_comp (id_appr, id_comp) values (47, 9);
insert into appr_comp (id_appr, id_comp) values (49, 4);
insert into appr_comp (id_appr, id_comp) values (40, 43);
insert into appr_comp (id_appr, id_comp) values (22, 47);
insert into appr_comp (id_appr, id_comp) values (8, 13);
insert into appr_comp (id_appr, id_comp) values (21, 12);
insert into appr_comp (id_appr, id_comp) values (10, 1);
insert into appr_comp (id_appr, id_comp) values (27, 39);
insert into appr_comp (id_appr, id_comp) values (4, 47);
insert into appr_comp (id_appr, id_comp) values (39, 7);
insert into appr_comp (id_appr, id_comp) values (13, 18);
insert into appr_comp (id_appr, id_comp) values (12, 33);