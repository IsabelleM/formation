# Questions

## Liste des apprenants
```SQL
SELECT name FROM apprenant;
```
![Réponse question 1](Question1.png)

## Nombre de compétences validées par apprenant
```SQL
SELECT apprenant.name, competences.id_comp AS competences FROM competences 
JOIN appr_comp ON appr_comp.id_comp = competences.id_comp 
JOIN apprenant ON apprenant.id_appr = appr_comp.id_appr WHERE competences.acquises = 1;
```
![Réponse question 2](Question2.png)

## Nombre d'absence (jours) par apprenant
```SQL
SELECT apprenant.name, COUNT(presence.id_pres) from apprenant
JOIN appr_pres ON apprenant.id_appr = appr_pres.id_appr
JOIN presence ON appr_pres.id_pres = presence.id_pres
GROUP BY apprenant.name;
```
![Réponse question 3](Question3.png)

## Nombre d'absence (jours) et nombre de compétences validées


## Tous les apprenants d'un groupe
```SQL
```

## Nombre de groupes auxquels chaque apprenant a participé
```SQL
SELECT apprenant.name, Count(groupe.id_group) FROM apprenant 
JOIN appr_group ON apprenant.id_appr = appr_group.id_appr 
JOIN groupe ON appr_group.id_group = groupe.id_group GROUP BY apprenant.name;
```
![Réponse question 5](Question5.png)

## Pour 1 apprenant (avec son nom), la liste des compétences et leur état (validé ou pas)

## Le plus absent qui doit ramener des pains au chocolat.

## Pour une date donnée la liste des présents.

## Liste des apprenants avec leur ordinateur / matériel

## Pour un projet, la liste des apprenants et leur groupe

## Pour chaque compétence le % de validation (si vous avez mis des niveaux, le niveau 3)